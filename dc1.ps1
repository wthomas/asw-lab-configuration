﻿param([string] $user, [string] $pass)

Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0

Set-Timezone -Id "Central European Standard Time"

Set-Service Audiosrv -StartupType Automatic

Add-Content "C:\Setup\01-domain-setup.ps1" -Value @"
New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "02-server-setup" -Value "Powershell.exe -NoProfile -NonInteractive -ExecutionPolicy Unrestricted -File C:\Setup\02-server-setup.ps1" -PropertyType String -Force

Import-Module ServerManager -Force
Install-WindowsFeature AD-Domain-Services,DNS,GPMC -IncludeManagementTools -IncludeAllSubFeature 

Add-DnsServerForwarder -IPAddress 168.63.129.16

Import-Module ADDSDeployment -Force
Install-ADDSForest -SkipPreChecks -SafeModeAdministratorPassword (ConvertTo-SecureString -AsPlainText "apob4t+6345TgW2" -Force) -CreateDnsDelegation:`$false -DomainMode "WinThreshold" -DomainName "contoso.com" -DomainNetbiosName "CONTOSO" -ForestMode "WinThreshold" -InstallDns:`$true -NoRebootOnCompletion:`$false -Force -ErrorAction Continue -WarningAction Continue 
"@

Add-Content "C:\Setup\02-server-setup.ps1" -Value @"
New-Item -ItemType "directory" -Path "C:\Profiles"
New-SmbShare -Name "Profiles" -Path "C:\Profiles" -FullAccess "Contoso\Domain Users"

Add-ADGroupMember "Remote Desktop Users" -Members "Domain Users"

Set-ADUser -Identity $User -ProfilePath "\\dc1.contoso.com\Profiles\$User" -UserPrincipalName "$User@contoso.com" -DisplayName "$User - Administrator"

Update-Help -ErrorAction SilentlyContinue -WarningAction SilentlyContinue

wget https://msedge.sf.dl.delivery.mp.microsoft.com/filestreamingservice/files/e2d06b69-9e44-45e1-bdf5-b3b827fe06b2/MicrosoftEdgeEnterpriseX64.msi -o c:\Setup\MicrosoftEdgeEnterpriseX64.msi
Start-Process msiexec.exe -Wait -ArgumentList '/i c:\Setup\MicrosoftEdgeEnterpriseX64.msi /qn /norestart'

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module PSReadLine -Force

Restart-Computer -Force
"@

$regpath = "HKLM:\Software\Microsoft\Windows\CurrentVersion"
New-Item -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion" -Name "RunOnce" -Force 
New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "01-domain-setup" -Value "Powershell.exe -NoProfile -NonInteractive -ExecutionPolicy Unrestricted -File C:\Setup\01-domain-setup.ps1" -PropertyType String -Force

$regpath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" 
New-ItemProperty -Path $regpath -Name 'AutoAdminLogon' -Value "1" -PropertyType String -Force 
New-ItemProperty -Path $regpath -Name 'DefaultUsername' -Value "$user" -PropertyType String -Force 
New-ItemProperty -Path $regpath -Name 'DefaultPassword' -Value "$pass" -PropertyType String -Force
New-ItemProperty -Path $regpath -Name 'AutoLogonCount' -Value 2 -PropertyType DWORD -Force

cmd /c "shutdown /r /t 10"
