param([string] $user, [string] $pass)

Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0

Set-Timezone -Id "Central European Standard Time"

Set-Service Audiosrv -StartupType Automatic

Add-Content "C:\Setup\01-join-domain.ps1" -Value @"
Add-LocalGroupMember "Remote Desktop Users" -Member "Authenticated Users"

Import-Module ServerManager -Force
Install-WindowsFeature RSAT-AD-Tools,RSAT-DNS-Server,GPMC -IncludeManagementTools -IncludeAllSubFeature -ErrorAction Continue -WarningAction Continue 

wget https://msedge.sf.dl.delivery.mp.microsoft.com/filestreamingservice/files/e2d06b69-9e44-45e1-bdf5-b3b827fe06b2/MicrosoftEdgeEnterpriseX64.msi -o c:\Setup\MicrosoftEdgeEnterpriseX64.msi
Start-Process msiexec.exe -Wait -ArgumentList '/i c:\Setup\MicrosoftEdgeEnterpriseX64.msi /qn /norestart'

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module PSReadLine -Force

Set-DnsClientServerAddress Ethernet -ResetServerAddresses

Write-Host "Waiting for DC1 to join domain" -NoNewLine

`$cred=New-Object System.Management.Automation.PSCredential ("contoso\$user" , (ConvertTo-SecureString -AsPlainText "$pass" -Force) )
do {
    Write-Host "." -NoNewline
    Start-Sleep 10 
    Add-Computer -DomainName contoso.com -Credential `$cred -ErrorAction SilentlyContinue -ErrorVariable ADJoinError
} while(`$ADJoinError)

Restart-Computer
"@

$regpath = "HKLM:\Software\Microsoft\Windows\CurrentVersion"
New-Item -Path $regpath -Name "RunOnce" -Force 
New-ItemProperty -Path "$regpath\RunOnce" -Name "01-join-domain" -Value "Powershell.exe -NoProfile -ExecutionPolicy Unrestricted -File C:\Setup\01-join-domain.ps1" -PropertyType String -Force

$regpath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" 
New-ItemProperty -Path $regpath -Name 'AutoAdminLogon' -Value "1" -PropertyType String -Force 
New-ItemProperty -Path $regpath -Name 'DefaultUsername' -Value "$user" -PropertyType String -Force 
New-ItemProperty -Path $regpath -Name 'DefaultPassword' -Value "$pass" -PropertyType String -Force
New-ItemProperty -Path $regpath -Name 'AutoLogonCount' -Value 1 -PropertyType DWORD -Force

cmd /c "shutdown /r /t 10"
